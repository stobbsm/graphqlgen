package graphqlgen

// graphqlgen Will be able to generate code and stubs based on generate tags
// Future plans will be to generate needed SQL statements for general CRUD operations

// The basis of the generation will be the text template engine (as with most code generators I've seen).
// The idea is to start with Structs, representing table/row definitions, and use tags
// to determine relationships and protected fields.

// The generated files should be able to be used as soon as they are generated, requiring no interaction to verify
// the basic functionality.

// GraphqlBasicScalar type represents different basic types in Graphql
// All structs should use these types
type GraphqlBasicScalar int

const (
	// Int is a 32bit integer as defined in the graphql schema
	Int GraphqlBasicScalar = iota + 1

	// Float is the same as a basic Golang float
	Float

	// String is the same as a basic Golang string
	String

	// Boolean is the same as a golang bool
	Boolean

	// Enum represents the special enum type, which will be translated to iota style in Golang
	Enum

	// ID will make use of the golang github.com/satori/go.uuid package
	// Every user defined struct needs an ID
	ID

	// Date represents a golang time.Time type, stored as a Date type in the database
	Date
)
