package graphqlgen

// Templates for common graphql fields
const (
	// GraphqlField set's up a graphql Field based on data found in the Struct
	GraphqlField = `"{{GraphqlField}}": &graphql.Field{
		{{GraphqlParams}}
	},`

	// GraphqlType holds the template for a graphql Type
	GraphqlType = `Type: {{GraphqlType}},`

	// GraphqlArgs creates the list of arguments
	GraphqlArgs = `Args: graphql.FieldConfigArgument{
		{{GraphqlArgsParams}}
	},`

	// GraphqlArgumentConfig creates the argument config statements
	GraphqlArgumentConfig = `"{{FieldName}}: &graphql.ArgumentConfig{
		{{GraphqlTypes}}
	},`

	// GraphqlResolver inserts a basic resolver for the struct
	GraphqlResolver = `Resolve: {{ObjectResolver}},`

	// GraphqlObject is the template for building a new Graphql Object
	GraphqlObject = `{{StructName}} = graphql.NewObject(graphql.ObjectConfig{
		Name: "{{StructName}},
		Fields: "graphql.FieldsThunk(func() graphql.Fields {
			return graphql.Fields{
				{{FieldsList}}
			}
		}),
	})`
)
