package graphqlgen

import (
	"errors"
	"runtime"
)

// Define common errors to be used by the code generator

// CalledError defines a generic error interface
type CalledError struct {
	err  error
	file string
	line int
}

// NewError creates a new generic error
func NewError(err error) *CalledError {
	_, file, line, _ := runtime.Caller(1)
	return &CalledError{
		err:  err,
		file: file,
		line: line,
	}
}

// NewErrorString let's you set the string of an error directly
func NewErrorString(err string) *CalledError {
	return NewError(errors.New(err))
}
